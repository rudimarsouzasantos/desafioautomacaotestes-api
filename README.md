# DesafioAutomacaoTestes-API 

Lista as simulações cadastradas.
Retorna HTTP Status 204 se não existir simulações cadastradas
	• Este código na API está retornando 200 e não 204 conforme documentado

Remover uma simulação
Remove uma simulação previamente cadastrada pelo seu ID.

Retorna o HTTP Status 204 se simulação for removida com sucesso
	• Este código na API está retornando 200 e não 204 conforme documentado

Retorna o HTTP Status 404 com a mensagem "Simulação não encontrada" se não
existir a simulação pelo ID informado
	• Este código na API está retornando 200 e não 404 conforme documentado

• Os CPFs estão sendo aceitos em qualquer formato
• A msg de cpf com restrição é "O CPF 00000000000 tem problema" e não "O CPF
99999999999 possui restrição" de acordo com o pdf
• Existe um erro ao consultar cpf sem restrição
• varios códigos de retorno não batem, fiz como achei que deveria estar
