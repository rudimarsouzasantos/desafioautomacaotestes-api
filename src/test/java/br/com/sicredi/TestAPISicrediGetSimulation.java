package br.com.sicredi;

import static io.restassured.RestAssured.*;
import io.restassured.http.ContentType;

import static org.hamcrest.Matchers.*;
import org.junit.Test;

import api.APISicredi;

public class TestAPISicrediGetSimulation extends APISicredi {

	
	@Test
	public void testGetOneSimulation() {
				
	    String cpf = "17822386034";
		
		given()
			.relaxedHTTPSValidation()
			.pathParam("paramCpf", cpf)
		.when()
			.get(getApiAcesses() + "{paramCpf}")
		.then()			
			.statusCode(getSucessCode())
			.contentType(ContentType.JSON)
		.assertThat()
			.body("cpf", equalTo(cpf))
				.and()
			.body("nome", equalTo("Deltrano"));
		}
	
	@Test
	public void testGetSimulationNonexistent() {
		
		String cpf = "xptoxpto000";

		given()
			.relaxedHTTPSValidation()
			.pathParam("paramCpf", cpf)
		.when()
			.get(getApiAcesses() + "{paramCpf}")
		.then()
			.statusCode(getErrorCodeNotFound())
			.contentType(ContentType.JSON)
		.assertThat()
			.body("mensagem", equalTo(("CPF " +cpf+ " n�o encontrado")));
		}
	
	
	
	@Test
	public void testGetAllSimulation() {
		
		given()
			.relaxedHTTPSValidation()			
		.when()
			.get(getApiAcesses())
		.then()
			.statusCode(getSucessCode())
			.contentType(ContentType.JSON)
		.assertThat()
			.body("nome", not(empty()))
				.and()
			.body("id", not(empty()))
				.and()
			.body("nome", not(empty()))
				.and()
			.body("cpf", not(empty()))
				.and()
			.body("email", not(empty()))
				.and()
			.body("valor", not(empty()))
				.and()
			.body("parcelas", not(empty()))
				.and()
			.body("seguro", not(empty()));
		}
	
	@Test
	public void testGetNonexistentSimulation() {
		
		given()
			.relaxedHTTPSValidation()			
		.when()
			.get(getApiAcesses())
		.then()			
			.statusCode(getCodeNotRegistred())
			.contentType(ContentType.JSON)
		.assertThat()
			.body(equalTo("[]"));
		}
	}
