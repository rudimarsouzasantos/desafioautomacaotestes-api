package br.com.sicredi;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.not;

import java.util.HashMap;
import java.util.Map;
import org.junit.Test;
import api.APISicredi;
import io.restassured.http.ContentType;
import model.User;

public class TestAPISicrediPostSimulation  extends APISicredi {

	User user = new User();
	Map<String,String> requestParams = new HashMap<String, String>();
	
	@Test
	public void testPostSimulation() {
		
		requestParams.put("nome", user.getNome());
		requestParams.put("cpf", user.getCpf());
		requestParams.put("email", user.getEmail());
		requestParams.put("valor", user.getValor());
		requestParams.put("parcelas", user.getParcelas());
		requestParams.put("seguro", user.getSeguro());

        given()
        	.relaxedHTTPSValidation()
        	.contentType(ContentType.JSON)
        	.body(requestParams)
        .when()
        	.post(getApiAcesses())
        .then()
        	.statusCode(getCodeSucessRegister())
        .assertThat()
        	.body("id", not(empty()))
        		.and()
	        .body("nome", equalTo(user.getNome()))
	        	.and()
	        .body("cpf", equalTo(user.getCpf()))
	        	.and()
	        .body("email", equalTo(user.getEmail()))
	        	.and()
	        .body("valor", equalTo(Integer.parseInt(user.getValor())))
	        	.and()
	        .body("parcelas", equalTo(Integer.parseInt(user.getParcelas())))
	        	.and()
	        .body("seguro", equalTo(Boolean.parseBoolean(user.getSeguro())));
	}
	
	@Test
	public void testPostSimulationCpfDuplicate() {
		
		requestParams.put("nome", user.getNome());
		requestParams.put("cpf", user.getCpf());
		requestParams.put("email", user.getEmail());
		requestParams.put("valor", user.getValor());
		requestParams.put("parcelas", user.getParcelas());
		requestParams.put("seguro", user.getSeguro());

        given()
        	.contentType(ContentType.JSON)
        	.body(requestParams)
        .when()
        	.post(getApiAcesses())
        .then()
        	.statusCode(getCodeCpfDuplicate())
        .assertThat()
        	.body("mensagem", equalTo("CPF duplicado"));
	}
	
	@Test
	public void testPostSimulationNameNull() {
		
		requestParams.put("nome", null);
		requestParams.put("cpf", user.getCpf());
		requestParams.put("email", user.getEmail());
		requestParams.put("valor", user.getValor());
		requestParams.put("parcelas", user.getParcelas());
		requestParams.put("seguro", user.getSeguro());

        given()
        	.contentType(ContentType.JSON)
        	.body(requestParams)
        .when()
        	.post(getApiAcesses())
        .then()
        	.statusCode(getCodeErrorField())
        .assertThat()
        	.body("erros.nome", equalTo(getMsgErrorNameNull()));
	}
	
	@Test
	public void testPostSimulationCpfNull() {
		
		requestParams.put("nome", user.getNome());
		requestParams.put("cpf", null);
		requestParams.put("email", user.getEmail());
		requestParams.put("valor", user.getValor());
		requestParams.put("parcelas", user.getParcelas());
		requestParams.put("seguro", user.getSeguro());

        given()
	        .contentType(ContentType.JSON)
	        .body(requestParams)
        .when()
        	.post(getApiAcesses())
        .then()
        	.statusCode(getCodeErrorField())
        .assertThat()
        	.body("erros.cpf", equalTo(getMsgErrorCpfNull()));
	}
	
	//Os CPFs est�o sendo aceitos em qualquer formato
	@Test
	public void testPostSimulationCpfUnformatted() {
		
		requestParams.put("nome", user.getNome());
		requestParams.put("cpf", "");
		requestParams.put("email", user.getEmail());
		requestParams.put("valor", user.getValor());
		requestParams.put("parcelas", user.getParcelas());
		requestParams.put("seguro", user.getSeguro());

        given()
	        .contentType(ContentType.JSON)
	        .body(requestParams)
        .when()
        	.post(getApiAcesses())
        .then()
        	.statusCode(getCodeErrorField())
        .assertThat()
        	.body("erros.cpf", equalTo(getMsgErrorCpfUnformatted()));
	}
	
	@Test
	public void testPostSimulationEmailNull() {
		
		requestParams.put("nome", user.getNome());
		requestParams.put("cpf", user.getCpf());
		requestParams.put("email", null);
		requestParams.put("valor", user.getValor());
		requestParams.put("parcelas", user.getParcelas());
		requestParams.put("seguro", user.getSeguro());

        given()
	        .contentType(ContentType.JSON)
	        .body(requestParams)
        .when()
        	.post(getApiAcesses())
        .then()
        	.statusCode(getCodeErrorField())
        .assertThat()
        	.body("erros.email", equalTo(getMsgErrorEmailNull()));
	}
	
	@Test
	public void testPostSimulationEmailUnformatted() {

		requestParams.put("nome", user.getNome());
		requestParams.put("cpf", user.getCpf());
		requestParams.put("email", "xptoxpto@");
		requestParams.put("valor", user.getValor());
		requestParams.put("parcelas", user.getParcelas());
		requestParams.put("seguro", user.getSeguro());

        given()
        	.contentType(ContentType.JSON)
        	.body(requestParams)
        .when()
        	.post(getApiAcesses())
        .then()
        	.statusCode(getCodeErrorField())
        .assertThat()
        	.body("erros.email", equalTo(getMsgErrorEmailUnformatted()));
	}
	
	@Test
	public void testPostSimulationInvalidEmail() {

		requestParams.put("nome", user.getNome());
		requestParams.put("cpf", user.getCpf());
		requestParams.put("email", "xptoxpto");
		requestParams.put("valor", user.getValor());
		requestParams.put("parcelas", user.getParcelas());
		requestParams.put("seguro", user.getSeguro());

        given()
	        .contentType(ContentType.JSON)
	        .body(requestParams)
        .when()
        	.post(getApiAcesses())
        .then()
        	.statusCode(getCodeErrorField())
        .assertThat()
        	.body("erros.email", equalTo(getMsgErrorInvalidEmail()));
	}
	
	@Test
	public void testPostSimulationValorNull() {
		
		requestParams.put("nome", user.getNome());
		requestParams.put("cpf", user.getCpf());
		requestParams.put("email", user.getEmail());
		requestParams.put("valor", null);
		requestParams.put("parcelas", user.getParcelas());
		requestParams.put("seguro", user.getSeguro());

        given()
	        .contentType(ContentType.JSON)
	        .body(requestParams)
        .when()
        	.post(getApiAcesses())
        .then()
        	.statusCode(getCodeErrorField())
        .assertThat()
        	.body("erros.valor", equalTo(getMsgErrorValorNull()));
	}
	
	@Test
	public void testPostSimulationParcelasNull() {
		
		requestParams.put("nome", user.getNome());
		requestParams.put("cpf", user.getCpf());
		requestParams.put("email", user.getEmail());
		requestParams.put("valor", user.getValor());
		requestParams.put("parcelas", null);
		requestParams.put("seguro", user.getSeguro());

        given()
	        .contentType(ContentType.JSON)
	        .body(requestParams)
        .when()
        	.post(getApiAcesses())
        .then()
        	.statusCode(getCodeErrorField())
        .assertThat()
        	.body("erros.parcelas", equalTo(getMsgErrorParcelasNull()));
	}
}
