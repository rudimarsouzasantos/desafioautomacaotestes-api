package br.com.sicredi;

import static io.restassured.RestAssured.*;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import org.junit.Test;
import api.APISicredi;
import model.User;

public class TestAPISicrediDeleteSimulation extends APISicredi {
	
	User user = new User();

	@Test
	public void testDeleteSimulationToId() {
		
		int id = 19;
	
		given()
			.pathParam("paramId", id)
		.when()
			.delete(getApiAcesses() + "{paramId}")
		.then()
			.statusCode(getSucessCode())
		.assertThat()
			.body(equalTo("OK"));		
		}
	
	@Test
	public void testDeleteSimulationToIdNonexistent() {
		
		int id = 0000;
	
		given()
			.pathParam("paramId", id)
		.when()
			.delete(getApiAcesses() + "{paramId}")
		.then()
			.statusCode(200)
		.assertThat()
			.body(equalTo("Simula��o n�o encontrada"));		
		}
}
