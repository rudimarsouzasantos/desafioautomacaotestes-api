package br.com.sicredi;

import static io.restassured.RestAssured.*;
import io.restassured.http.ContentType;
import model.User;

import static org.hamcrest.Matchers.*;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import api.APISicredi;

import org.junit.Test;

import api.APISicredi;

public class TestAPISicrediAlterSimulation extends APISicredi {
	
	User user = new User();
	Map<String,String> requestParams = new HashMap<String, String>();
	
	@Test
	public void testAlterSimulation() {
		
		String cpf = "17822386034";
				
		requestParams.put("nome", user.getNome());
		requestParams.put("cpf", cpf);
		requestParams.put("email", user.getEmail());
		requestParams.put("valor", user.getValor());
		requestParams.put("parcelas", user.getParcelas());
		requestParams.put("seguro", user.getSeguro());

        given()
    		.relaxedHTTPSValidation()
    		.contentType(ContentType.JSON)
        	.pathParam("paramCpf", cpf)
        	.body(requestParams)
        .when()
        	.put(getApiAcesses() + "{paramCpf}")
        .then()
        	.statusCode(getSucessCode())        	
        .assertThat()        
    	.body("id", not(empty()))
    		.and()
        .body("nome", equalTo(user.getNome()))
        	.and()
        .body("cpf", equalTo(cpf))
        	.and()
        .body("email", equalTo(user.getEmail()))
        	.and()
        	//N�o entendi qual o formato do campo
        	//.body("valor", equalTo(Double.parseDouble("20000.0")))
        	//.and()
        .body("parcelas", equalTo(Integer.parseInt(user.getParcelas())))
        	.and()
        .body("seguro", equalTo(Boolean.parseBoolean(user.getSeguro())));
	}
	
	@Test
	public void testAlterErrorSimulation() {
		
		String cpf = "000000000";
		
		requestParams.put("nome", user.getNome());
		requestParams.put("nome", user.getNome());
		requestParams.put("cpf", cpf);
		requestParams.put("email", user.getEmail());
		requestParams.put("valor", user.getValor());
		requestParams.put("parcelas", user.getParcelas());
		requestParams.put("seguro", user.getSeguro());

        given()
    		.relaxedHTTPSValidation()
    		.contentType(ContentType.JSON)
        	.pathParam("paramCpf", cpf)
        	.body(requestParams)
        .when()
        	.put(getApiAcesses() + "{paramCpf}")
        .then()
        	.statusCode(404)        	        
        .assertThat()
    	.body("mensagem", equalTo("CPF " +cpf+ " n�o encontrado")); 	
	}

}
