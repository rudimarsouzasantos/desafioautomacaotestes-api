package br.com.sicredi;

import static io.restassured.RestAssured.*;
import io.restassured.http.ContentType;

import static org.hamcrest.Matchers.*;
import org.junit.Test;

import api.APISicredi;

public class TestAPISicrediGetRestriction extends APISicredi {

	@Test
	public void testGetSimulationRestriction() {
		
		String cpf = "60094146012";

		given()
			.relaxedHTTPSValidation()
			.pathParam("paramCpf", cpf)
		.when()
			.get(getApiAcessesRetriction() + "{paramCpf}")
			.then()
			.statusCode(getSucessCode())
			.contentType(ContentType.JSON)
			.assertThat()
				.body("mensagem", equalTo(("O CPF " +cpf+ " tem problema")));
		}
	
	//Existe um erro ao consultar cpf sem restri��o
	@Test
	public void testGetSimulationUnRestriction() {
		
		String cpf = "17822386034";

		given()
			.relaxedHTTPSValidation()
			.pathParam("paramCpf", cpf)
		.when()
			.get(getApiAcessesRetriction() + "{paramCpf}")
			.then()
			.statusCode(204)
			.contentType(ContentType.JSON)
			.assertThat()
				.body("mensagem", equalTo(("O CPF " +cpf+ " tem problema")));
		}

}
