package api;

public class APISicredi {
	
	String apiAcesses = "http://localhost:8080/api/v1/simulacoes/";
	String apiAcessesRetriction = "http://localhost:8080/api/v1/restricoes/";
	int errorCodeNotFound = 404;
	int sucessCode = 200;
	int codeNotRegistred = 204;
	int codeSucessRegister = 201;
	int codeCpfDuplicate = 400;
	int codeErrorField = 400;
	String msgErrorNameNull = "Nome n�o pode ser vazio";
	String msgErrorCpfNull = "CPF n�o pode ser vazio";
	String msgErrorCpfUnformatted = "CPF n�o pode ser vazio";
	String msgErrorEmailNull = "E-mail n�o deve ser vazio";
	String msgErrorInvalidEmail = "E-mail deve ser um e-mail v�lido";
	String msgErrorEmailUnformatted = "E-mail deve ser um e-mail v�lido";
	String msgErrorValorNull = "Valor n�o pode ser vazio";
	String msgErrorParcelasNull = "Parcelas n�o pode ser vazio";
	String msgErrorSeguroNull = "Seguro n�o pode ser vazio";
	
	
	public String getApiAcessesRetriction() {
		return apiAcessesRetriction;
	}


	public int getCodeErrorField() {
		return codeErrorField;
	}


	public String getMsgErrorInvalidEmail() {
		return msgErrorInvalidEmail;
	}


	public String getMsgErrorValorNull() {
		return msgErrorValorNull;
	}


	public String getMsgErrorParcelasNull() {
		return msgErrorParcelasNull;
	}


	public String getMsgErrorSeguroNull() {
		return msgErrorSeguroNull;
	}


	public String getMsgErrorEmailUnformatted() {
		return msgErrorEmailUnformatted;
	}


	public String getMsgErrorEmailNull() {
		return msgErrorEmailNull;
	}


	public String getMsgErrorCpfUnformatted() {
		return msgErrorCpfUnformatted;
	}


	public String getMsgErrorCpfNull() {
		return msgErrorCpfNull;
	}


	public String getMsgErrorNameNull() {
		return msgErrorNameNull;
	}


	public int getCodeCpfDuplicate() {
		return codeCpfDuplicate;
	}


	public int getCodeSucessRegister() {
		return codeSucessRegister;
	}


	public int getCodeNotRegistred() {
		return codeNotRegistred;
	}


	public int getSucessCode() {
		return sucessCode;
	}


	public int  getErrorCodeNotFound() {
		return errorCodeNotFound;
	}


	public String getApiAcesses() {
		return apiAcesses;
	}
	
	

}
